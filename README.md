# not-a-kfc-copy

Тестовое задание - сделать страничку по макету, и немного функционала к ней. Дизайн что-то напоминает, но никак не пойму что. 
Есть бэкенд, который умеет отдавать json с бургерами и принимать json с корзиной (и писать его в консоль).

[Деплой](https://dk-mcdonalds.herokuapp.com/)

## Стэк

* Typescript
* React
* Redux
* Redux-toolkit
* axios
* Formik
* Yup
* React-scroll
* react-store-badge
* SASS
* express
